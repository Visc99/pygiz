"""
x = [10, 20, 30]
y = [20, 30, 0]
z = []

# for every ite in x, multiply by every number in y and append it to z.

# nested for loop
for i in x:
    for j in y:
        z.append(i * j)
"""

invitations = []
x = ['fjoraldo', 'fjorela', 'edison', 'frank', 'ana', 'johnny']

# add only people whose name starts with f or ends with a. if the name starts in e, add it to the invitations
# if, elif, else

for name in x:
    if name[0] == 'f' or name[0] == 'e':
        invitations.append(name)
    elif name[-1] == 'a':
        break
    else:
        print(name, 'is not invited.')

print(invitations)



