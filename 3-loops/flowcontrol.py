
"""

Generally speaking, your script will run from the top of the file to the bottom.

However, there's a few ways to alter the flow of the program. We can use things like loops and conditionals statements for this.

A loop is simply an instruction that runs until a break condition is met.

We'll be looking at two loops in Python, for and while loops.

"""

count = 0

while count < 10:
    print(count)
    count += 1 # this is the same as count = count + 1

"""

Another kind of loop is the for loop, which is more useful.

"""

x = [1, 2, 3]

for i in x:
    print(i)

# Notice that i represents the item you're iterating over. You can change the name of this variable to anything.



