
# Create a RPS game where you play against an AI until someones reaches 3 points.

# The AI makes a random choice between Rock Paper or Scissors
# The human player makes a choice using the input() function
# Run the game in a loop until someones reaches 3 points.
# At every iteration, print the human's choice and the AI's choice, along with the result.
# When you're finished, let me know and we'll push it to Gitlab. Message me in the chat.

import random

choices = ['r', 'p', 's']
triangle = {'r': 's', 's': 'p', 'p':'r'}

aiscore = 0
playerscore = 0

while aiscore < 3 and playerscore < 3:

    aichoice = random.choice(choices)
    playerchoice = input('Please select a hand ')

    print(f'AI: {aichoice}, Player: {playerchoice}')

    if aichoice == playerchoice:
        print('Tie!')

    elif triangle[playerchoice] == aichoice:
        print('You win!')
        playerscore += 1

    else:
        print('AI wins')
        aiscore += 1

if aiscore > playerscore:
    print('AI wins the game')

else:
    print('You win the game!')



x = 'hello'
y = 'world'

print(x, y)
print(x + ' ' + y)
print('{} {}'.format(x, y))
print(f'{x} {y}')













