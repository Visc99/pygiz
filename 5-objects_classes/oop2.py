
"""
x = ['Johnny', 30]
y = ['Endri',  25]

x.insert(0, 'USA')

z = [x, y]

for person in z:
    print(f'Hello, my name is {person[0]} and I am {person[1]} years old')

"""

x = {'name': 'johnny'}
y = {'name': 'endri'}

z = [x, y]

for person in z:
    print(f'Hello, my name is {person["name"]}')


class Person:

    # constructor
    def __init__(self, name, age):
        self.name = name
        self.age = age
        self.country = 'Albania'

x = Person('Johnny', 30)

print(x.name)




