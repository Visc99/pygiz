
This is a non-exhaustive list of useful Git commands you will benefit from learning.
Anything inside of <> should be replaced with an actual argument. When in doubt,
feel free to google any specific command.


# To copy an existing repo or to create a new one.
git clone <link>
git init
git remote add origin <link>

# Preparing and pushing changes
git add <files>
git commit -m "message"
git push

# To update your local branch
git fetch
git merge
git pull (basically fetch and merge)

# To view your branches and to delete branches.
git branch
git branch -d <branch_name>
git branch -D <branch_name>

# For switching or creating branches.
git checkout <existing_branch>
git checkout -b <new_branch_name>

# To get useful information
git status
git show
git diff
git log
git hist

# To store your git credentials so you don't have to provide them everytime you push
git config --global credential.helper "cache --timeout=3600"

# To locally save current changes, but revert to a clean tree
git stash

# Discards current changes and reverts to tip of the branch. CAREFUL!
git reset --hard HEAD

RECOMMENDED WORKFLOW:

git status
git add .
git commit -m "Commit msg"
git pull --rebase origin <branch_name>
git push origin <branch_name>

Note: Rebase pulls your remote branch, updates your current branch (if there are any remote updates),
and puts your current commit ON TOP.

