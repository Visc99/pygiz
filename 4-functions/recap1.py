

def get_average(numlist):
    # Given a list of numbers, return the average of the total.

    total = 0
    for n in numlist:
        total += n

    average = total/len(numlist)

    return average

x = [10, 20, 30]

get_average(x)

def thisthat(number):
    # For any given number, if the number is divisible 3, return 'this'
    # If it's divisible by 5, return 'that'
    # If it's divisible by 3 and 5, return 'thisthat'
    # If divisible by neither, return the number

    return

def youngest_member(family):
    # Given a set of dictionaries, where the key is a name of a family
    # member and the value is the age, return the name of the youngest member.

    youngest = 200
    youngest_name = ''

    for member in family:
        for name, age in member.items():
            if age < youngest:
                youngest = age
                youngest_name = name

    return youngest_name, youngest

x = [{'jimmy': 20},
    {'bobby': 15},
    {'skanderbeg': 150},
    {'ana': 30},
    {'baby': 5}]

print(youngest_member(x))

def net_score(games):
    # Given a set of lists, where each list contains a pair of scores in
    # soccer matches between a local team and a foreign team, return
    # the total difference between goals scored and goals allowed.

    return


