# Workflow from app creation to deployment:

This guide will walk you through the process of setting up a Gitlab pipeline to automate deployments to Heroku. By doing this once, everytime you push or merge to your master branch, your app will be deployed to Heroku.

## Steps:

0. Set up your virtual environment
1. Create and prepare your Flask project
2. Prepare files we'll need for deployment
3. Create a Heroku app
4. Create a Gitlab project for your app
5. Configure your Gitlab variables
6. Initialize and push your local repository

## 0. Set up your environment

This first step is optional, but it makes preparing your application for deployment much easier. Setting up your environment often implies creating a virtual environment that we will use to keep track of dependencies our project will need.

First, make a new folder with any name you want and navigate to it:

```console
mkdir mynewapp
cd mynewapp
```

Now, let's create a virtual environment with the venv library:

```console
python3.7 -m venv env
```

Activate it with the source command and the path to the activate script:

```console
source env/bin/activate
```
If this worked, you should now see (env) at the beginning of your bash prompt.

## 1. Create and prepare your Flask project


While in your virtual environment is activated, install flask and any other package your app depends on:

```console
pip install flask
```

Then, create flask_app.py:


```python
from flask import Flask


@app.route('/')
def hello_world():
    return "Hello, I have been deployed successfully."


if __name__ == '__main__':
    app.run(threaded=True, port=5000)
```

## 2. Prepare files we'll need for deployment

Now we're going to need a few files in our current folder to make sure our deploy is perfect.

We'll need four files: .gitignore, .gitlab-ci.yml, Procfile, and requirements.txt

.gitignore is used to tell git which files should not be tracked in our history. Add this for now:

```
env
```

All we're doing here is making sure our virtual environment, which includes Python and its standard library, does not get added to our repository.

Now, let's create our requirements.txt file, which is used to keep track of our app's dependencies. Since we we're using a virtual environment, this is very simple:

```console
pip freeze > requirements.txt
```

Open the newly created requirements.txt file and add this line at the end:

```
gunicorn
```

Gunicorn is an HTTP server that will run our Flask app in Heroku.

Next, let's create a Procfile with the following line:

```
web: gunicorn flask_app: app
```

This will tell Heroku to load the app instance inside flask_app.py running a web server using gunicorn. It's a requirement specific to Heroku deployments.

Finally, let's add our .gitlab-ci.yml file:

```
stages:
  - deploy

heroku_deploy:
  image: "python:3.7"
  stage: deploy
  script:
  - apt-get update -qy
  - apt-get install -y ruby-dev
  - gem install dpl
  - dpl --provider=heroku --app=$HEROKU_APP_PRODUCTION --api-key=$HEROKU_API_KEY
  only:
  - master
```

This file will enable Gitlab pipelines for us when we push this repository to Gitlab. A pipeline is made up jobs and stages. We can group jobs into different stages, and jobs of the same stage are executed in parallel.

The code above will install the Ruby language on a Gitlab runner (which runs on a Debian distribution of Linux), install a package (or a Ruby gem) called dpl, and we use that package to deploy to Heroku. Two important things to note here are two variables: HEROKU_APP_PRODUCTION and HEROKU_API_KEY. They are preceded by the $ symbol, which tells us they're Gitlab variables, so in practice, these two variables have values that we can assign outside of our code, but inside of our Gitlab environment.

## 3. Create a Heroku app

Go to your dashboard on Heroku, click the New button, and select an app. Name it anything you want, we'll be using this name shortly. Let's assume we'll call this app myflaskapp

## 4. Create your Gitlab project

This is pretty self-explanatory. Just go your Gitlab account and create a new project with any name you want.

## 5. Configure your Gitlab environment variables

Now that we have this, let's add our environment variables to Gitlab. Go to Settings, CI-CD, Variables and create two:

HEROKU_API_KEY and HEROKU_APP_PRODUCTION

The value of the first is the secret API key that you can find in your Heroku account page.

The value of the second is the name of the Heroku app you created.

The last thing we need to do now after saving this variables is deploy our application to Gitlab.

## 6. Initialize and push local repository

On Pythonanywhere (or wherever your project is located), navigate to your folder, and in a bash console, type this to create a git repo and push to your Gitlab project:

```console
git init
git remote add origin <replace this with your Gitlab project address>
git add .
git commit -m "Initial commit"
git push origin master
```

Now go to myflaskapp.herokuapp.com (replace myflaskapp with the actual name of your Heroku app) and you now see a message saying "Hello, I have been deployed successfully."

If it works, you're done! Now, everytime you push to master, a pipeline is triggered that will run the code that you put in your .gitlab-ci.yml file.

