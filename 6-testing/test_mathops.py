
from mathops import MathOps


def test_add():
    assert MathOps.add(10, 10) == 20

def test_subtract():
    assert MathOps.subtract(10, 10) == 0

def test_multiply():
    assert MathOps.multiply(5, 5, 10) == 250
